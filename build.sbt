import com.typesafe.sbt.packager.docker._

val microserviceName = "bineer"
name := microserviceName

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.8"

libraryDependencies += jdbc
libraryDependencies += cache
libraryDependencies += ws
libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "1.5.1" % Test

packageName in Docker := "llfrometa/" + microserviceName
version in Docker := sys.props.getOrElse("version", default = "dev")

dockerCommands := Seq(
  Cmd("FROM", "openjdk:alpine"),
  Cmd("RUN", "apk add --update bash && rm -rf /var/cache/apk/*"),
  Cmd("WORKDIR", "/opt/docker"),
  Cmd("ADD", "opt /opt"),
  Cmd("ENTRYPOINT", "bin/" + microserviceName, ""),
  ExecCmd("CMD", "bin/" + microserviceName)
)
